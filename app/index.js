import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import Home from './screens/Home';

const newLocal = '#4f6d7a';

EStyleSheet.build({
  $primaryBlue: newLocal,
  $white: '#FFFFFF',
});


export default () => <Home />;
